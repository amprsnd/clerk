class CategoriesController < ApplicationController
  def index
    @categories = Category.all
  end

  def show
    @category = Category.find params[:id]
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new category_params

    if @category.save
      flash[:notice] = 'Category created'
      redirect_to categories_path
    else
      flash.now[:warning] = 'There were problems when trying to create a new category'
      render :action => :new
    end
  end

  def edit
    @category = Category.find params[:id]
  end

  def update
    @category = Category.find params[:id]

    if @category.update_attributes category_params
      flash[:notice] = 'Category has been updated'
      redirect_to categories_path
    else
      flash.now[:warning] = 'There were problems when trying to update this category'
      render :action => :edit
    end
  end

  def destroy
    @category = Category.find params[:id]

    @category.destroy
    flash[:notice] = 'Category has been deleted'
    redirect_to categories_path
  end

  private

  def category_params
    params.require(:category).permit(:title, :description)
  end
end
