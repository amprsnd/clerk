class PlatformsController < ApplicationController
  def index
    @platforms = Platform.all
  end

  def show
    @platform = Platform.find params[:id]
  end

  def new
    @platform = Platform.new
  end

  def create
    @platform = Platform.new platform_params

    if @platform.save
      flash[:notice] = 'Platform created'
      redirect_to platforms_path
    else
      flash.now[:warning] = 'There were problems when trying to create a new platform'
      render action: :new
    end
  end

  def edit
    @platform = Platform.find params[:id]
  end

  def update
    @platform = Platform.find params[:id]

    if @platform.update_attributes platform_params
      flash[:notice] = 'Platform has been updated'
      redirect_to platforms_path
    else
      flash.now[:warning] = 'There were problems when trying to update this platform'
      render action: :edit
    end
  end

  def destroy
    @platform = Platform.find params[:id]

    @platform.destroy
    flash[:notice] = 'Platform has been deleted'
    redirect_to platforms_path
  end

  def start

    @platform = Platform.find params[:id]
    interval = "#{@platform.interval}s"

    ENV['TZ'] = 'Asia/Astana'
    job_id =
        Rufus::Scheduler.singleton.every interval do

          Show.visit(@platform)

        end

    puts "scheduled job #{job_id}"
    redirect_to platforms_path

  end

  private

  def platform_params
    params.require(:platform).permit(:title, :link, :description, :timeout, :interval)
  end
end
