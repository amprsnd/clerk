class TagsController < ApplicationController
  def index
    @tags = Tag.all
  end

  def show
    @tag = Tag.find params[:id]
  end

  def new
    @tag = Tag.new
  end

  def create
    @tag = Tag.new tag_params

    if @tag.save
      flash[:notice] = 'Tag created'
      redirect_to tags_path
    else
      flash.now[:warning] = 'There were problems when trying to create a new tag'
      render action: :new
    end
  end

  def edit
    @tag = Tag.find params[:id]
  end

  def update
    @tag = Tag.find params[:id]

    if @tag.update_attributes tag_params
      flash[:notice] = 'Tag has been updated'
      redirect_to tags_path
    else
      flash.now[:warning] = 'There were problems when trying to update this tag'
      render action: :edit
    end
  end

  def destroy
    @tag = Tag.find params[:id]

    @tag.destroy
    flash[:notice] = 'Tag has been deleted'
    redirect_to tags_path
  end

  private

  def tag_params
    params.require(:tag).permit(:title, :description)
  end
end
