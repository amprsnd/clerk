class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # variable for opened platforms in browser
  # open in TasksController
  # close in CollectorsController

end
