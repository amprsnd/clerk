class Place < ApplicationRecord

  # relations
  has_many :banners
  has_many :shows

  belongs_to :platform

  # validations

end
