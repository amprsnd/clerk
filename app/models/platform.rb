class Platform < ApplicationRecord

  # relations
  has_many :places
  has_many :banners

  has_many :shows

  # validations

end
