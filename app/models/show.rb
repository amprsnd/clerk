class Show < ApplicationRecord

  # relations
  belongs_to :platform
  belongs_to :place
  belongs_to :banner
  belongs_to :client

  # validations


  # methods
  def self.visit(platform)

    @current_platform = platform
    @timeout = 3000

    # platform

    # one visit
    @visit = `phantomjs ./vendor/collector.js #{@current_platform.link} #{@current_platform.timeout}`
    visit = @visit.split('=======')
    visit_result = JSON.parse visit[1]


    # Each visit results
    visit_result.each do |show|

      #puts '====='
      #puts banner_hash
      #puts show[0]    # ID
      #puts show[1]    # Name
      #puts show[2]    # Width
      #puts show[3]    # Height
      #puts show[4]
      #puts show[5]
      #puts show[6][0] # google_ads_iframe_
      #puts show[6][1] # google_id
      #puts show[6][2] # place_name
      #puts show[7]    # html
      #puts show[8]    # base64 screenshot
      #puts '====='

      # Place
      @current_place = Place.find_or_create_by(html_id: show[0]) do |place|
        place.title         = show[0]
        place.html_id       = show[0]
        place.html_name     = show[1]
        place.html_title    = show[6][2]
        place.width         = show[2]
        place.height        = show[3]
        place.google_id     = show[6][1]
        place.google_title  = show[6][2]
        place.platform_id   = @current_platform.id
      end

      #client
      # TODO: extract client link
      @current_client = Client.first

      # Banner
      #links = show[7].match(/(http|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])/)
      #html = Nokogiri::HTML(show[7])
      #links = html.css('a')

      #find or create banner

      banner_hash = Digest::SHA256.hexdigest show[8]
      @current_banner = Banner.find_or_create_by(html: show[7])
      if @current_banner.new_record?
        @current_banner.width   = show[2]
        @current_banner.height  = show[3]
        @current_banner.title   = "#{@current_platform.title} - #{@current_place.google_title} (#{@current_banner.width}x#{@current_banner.height})"
        @current_banner.html    = show[7]
        @current_banner.screenshot = show[8]

        @current_banner.platform_id  = @current_platform.id
        @current_banner.place_id     = @current_place.id
        @current_banner.client_id    = @current_client.id

        @current_banner.hash_line = banner_hash
        @current_banner.save
      end

      # create one show

      @current_show = Show.new(
          platform_id:  @current_platform.id,
          place_id:     @current_place.id,
          client_id:    @current_client.id,
          banner_id:    @current_banner.id
      )
      @current_show.save

      puts '+++++++++'
      puts "Platform: #{@current_platform.errors.full_messages}"
      puts "Place: #{@current_place.errors.full_messages}"
      puts "Banner: #{@current_banner.errors.full_messages}"
      puts "Show: #{@current_show.errors.full_messages}"
      puts '+++++++++'

    end

    puts '=== One show ==='

  end

  def self.find_uniqueness(html_string)

    uniqueness = ''

    html = Nokogiri::HTML(html_string)

    images  = html.css('img')
    links   = html.css('a')
    frames  = html.css('iframe')
    scripts = html.css('script')

    images.each do |i|

      uniqueness << i.attr(src)

    end



=begin
    useful_frames = 0

    frames.each do |f|

      unless f.attr('src') == '' || f.attr('src').include?('/safeframe/')

        useful_frames = 1

      end

    end
=end


    if images.length == 0

      if frames == 0

        if links.length == 0

          if scripts.length == 0

            # no uniqueness...

          else
            # uniq is script
            uniqueness = 'uniqueness is Script'
          end

        else
          # uniq is frame
          uniqueness = 'uniqueness is Link'
        end

      else
        # uniq is link
        uniqueness = 'uniqueness is Frame'
      end

    else
      # uniq is image
      uniqueness = 'uniqueness is Image'
    end

    # return uniqueness

  end

end
