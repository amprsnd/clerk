class Banner < ApplicationRecord

  # relations
  belongs_to :platform
  belongs_to :place
  belongs_to :client

  has_many :shows

  # validations

end
