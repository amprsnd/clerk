Rails.application.routes.draw do

  root 'static#index'

  # get 'static/about'
  # get 'static/contacts'

  resources :clients

  resources :categories
  resources :tags

 #resources :bannerz, only: [:index]
  match 'bannerz', to: 'banners#index', via: :get, as: 'banners_index'


  resources :platforms
  match 'platforms/:id/start/', to: 'platforms#start', via: :get, as: 'platform_start'

  resources :places


  #resources :shows

  resources :statuses, only: [:index, :show]

end
