// Init
var page = new WebPage();
var system = require('system');

//args
var address = system.args[1];
var timeout = system.args[2];

// Settings
page.viewportSize = {
    width: 1860,
    height: 990
};
page.settings.webSecurityEnabled = false;
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.109 Safari/537.36';

// Events
page.onConsoleMessage = function (msg, line, source) {
    //console.log('console> ' + msg);
};
page.onAlert = function (msg) {
    //console.log('alert!!> ' + msg);
};

page.open(address, function (status) {

    if (status === 'success') {

        page.injectJs('assets/javascript/jquery.js');

        var result = page.evaluate(function () {

            console.clear();

            var $j = jQuery.noConflict();
            var banners = [];
            // TODO: selectors to params
            var frames = $j('iframe[id^="google_ads_"]');

            frames.each(function (i, f) {

                var offset = $j(f).offset();
                var html = $j(f).contents().find('html').html();
                banners[i] = [];

                banners[i][0] = $j(f).attr('id');
                if ($j(f).attr('name') == '') { $j(f).attr('name', $j(f).attr('id')); }
                banners[i][1] = $j(f).attr('name');
                banners[i][2] = $j(f).attr('width');
                banners[i][3] = $j(f).attr('height');
                banners[i][4] = Math.round(offset.left);
                banners[i][5] = Math.round(offset.top);
                // banner content
                banners[i][6] = html;

            });

            return banners;

        });

        var bannersData = [];

        setTimeout(function () {

            for (var i = 0; i < result.length; i++) {

                //page.switchToFrame(result[i][1]);

                //var content = page.frameContent;
                var content = result[i][6];

                if (content.length > 75) {

                    bannersData.push([]);
                    var oneShow = bannersData[bannersData.length - 1];

                    page.clipRect = {
                        top: result[i][5],
                        left: result[i][4],
                        width: result[i][2],
                        height: result[i][3]
                    };

                    oneShow.push(
                        result[i][0],
                        result[i][1],
                        result[i][2],
                        result[i][3],
                        result[i][4],
                        result[i][5],
                        result[i][0].split('/'),
                        content,
                        'data:image/png;base64,' + page.renderBase64('PNG')
                    );

                }
                //page.switchToMainFrame();
            }
            console.log('garbage');
            console.log('=======');
            console.log(JSON.stringify(bannersData));
            phantom.exit();

        }, timeout);


    } else {
        console.log('platform error');
        phantom.exit();
    }

});