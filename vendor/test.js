// Init
var page = new WebPage();
var system = require('system');

//args
var address = system.args[1];
var timeout = system.args[2];

// Settings
page.viewportSize = {
    width: 1860,
    height: 990
};
page.settings.webSecurityEnabled = false;
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.109 Safari/537.36';

// Events
page.onConsoleMessage = function (msg, line, source) {
    //console.log('console> ' + msg);
};
page.onAlert = function (msg) {
    //console.log('alert!!> ' + msg);
};

page.open(address, function (status) {

    if (status === 'success') {

        setTimeout(function () {

            page.injectJs('assets/javascript/jquery.js');

            var result = page.evaluate(function () {

                var $j = jQuery.noConflict();

                return $j('html').html();

            });

            console.log('garbage');
            console.log('=======');

            console.clear();

            console.log(result);
            phantom.exit();

        }, timeout);


    } else {
        console.log('platform error');
        phantom.exit();
    }

});