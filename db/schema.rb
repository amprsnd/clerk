# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170622052303) do

  create_table "banners", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "width"
    t.integer  "height"
    t.string   "hash_line"
    t.integer  "client_id"
    t.integer  "platform_id"
    t.integer  "place_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.text     "html",        limit: 65535
    t.text     "screenshot",  limit: 65535
    t.index ["client_id"], name: "index_banners_on_client_id", using: :btree
    t.index ["place_id"], name: "index_banners_on_place_id", using: :btree
    t.index ["platform_id"], name: "index_banners_on_platform_id", using: :btree
  end

  create_table "categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["category_id"], name: "index_categories_on_category_id", using: :btree
  end

  create_table "clients", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "title"
    t.string   "description"
    t.string   "link"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "crono_jobs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "job_id",                               null: false
    t.text     "log",               limit: 4294967295
    t.datetime "last_performed_at"
    t.boolean  "healthy"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.index ["job_id"], name: "index_crono_jobs_on_job_id", unique: true, using: :btree
  end

  create_table "places", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "title"
    t.string   "description"
    t.string   "html_id"
    t.string   "html_title"
    t.string   "html_name"
    t.integer  "width"
    t.integer  "height"
    t.string   "google_id"
    t.string   "google_title"
    t.string   "type"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "platform_id"
    t.index ["platform_id"], name: "index_places_on_platform_id", using: :btree
  end

  create_table "platforms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "title"
    t.string   "description"
    t.string   "link"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "timeout",     default: 3000
    t.integer  "interval",    default: 60
  end

  create_table "shows", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "client_id"
    t.integer  "platform_id"
    t.integer  "place_id"
    t.integer  "banner_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["banner_id"], name: "index_shows_on_banner_id", using: :btree
    t.index ["client_id"], name: "index_shows_on_client_id", using: :btree
    t.index ["place_id"], name: "index_shows_on_place_id", using: :btree
    t.index ["platform_id"], name: "index_shows_on_platform_id", using: :btree
  end

  create_table "tags", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_foreign_key "banners", "clients"
  add_foreign_key "banners", "places"
  add_foreign_key "banners", "platforms"
  add_foreign_key "categories", "categories"
  add_foreign_key "places", "platforms"
  add_foreign_key "shows", "banners"
  add_foreign_key "shows", "clients"
  add_foreign_key "shows", "places"
  add_foreign_key "shows", "platforms"
end
