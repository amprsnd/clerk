class CreatePlaces < ActiveRecord::Migration[5.0]
  def change
    create_table :places do |t|
      t.string :title
      t.string :description
      t.string :html_id
      t.string :html_title
      t.string :html_name
      t.integer :width
      t.integer :height
      t.string :google_id
      t.string :google_title
      t.string :type

      t.timestamps
    end
  end
end
