class FixBannerHashColumnName < ActiveRecord::Migration[5.0]
  def self.up
    rename_column :banners, :hash, :hash_line
  end

  def self.down
    rename_column :banners, :hash_line, :hash
  end
end
