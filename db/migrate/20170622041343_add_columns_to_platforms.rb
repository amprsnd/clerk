class AddColumnsToPlatforms < ActiveRecord::Migration[5.0]
  def change
    add_column :platforms, :timeout, :integer, default: '3000'
    add_column :platforms, :interval, :integer, default: '60'
  end
end
