class AddColumnsToBanners < ActiveRecord::Migration[5.0]
  def change
    add_column :banners, :html, :text, limit: 16.megabytes - 1
    add_column :banners, :screenshot, :text, limit: 16.megabytes - 1
    remove_column :banners, :filepath
  end
end
