class CreateShows < ActiveRecord::Migration[5.0]
  def change
    create_table :shows do |t|
      t.references :client, foreign_key: true
      t.references :platform, foreign_key: true
      t.references :place, foreign_key: true
      t.references :banner, foreign_key: true

      t.timestamps
    end
  end
end
