class CreateBanners < ActiveRecord::Migration[5.0]
  def change
    create_table :banners do |t|
      t.string :title
      t.string :description
      t.integer :width
      t.integer :height
      t.string :hash
      t.string :filepath
      t.references :client, foreign_key: true
      t.references :platform, foreign_key: true
      t.references :place, foreign_key: true

      t.timestamps
    end
  end
end
